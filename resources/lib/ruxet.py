# -*- coding: utf-8 -*-

import xbmc
import xbmcaddon
import xbmcgui
import xbmcplugin

import re
import requests
import urlparse

from HTMLParser import HTMLParser
from htmlentitydefs import name2codepoint

from urllib import urlencode


ADDON_ID = 'plugin.video.ruxet'

class HtmlElement(object):
    def __init__(self, attributes):
        self.attributes = {}
        for attribute in attributes:
            self.attributes[attribute[0]] = attribute[1]


class Link(HtmlElement):
    def __init__(self, attributes):
        super(Link, self).__init__(attributes)
        self.image = None

    def add_image(self, image):
        self.image = image


class Parser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.current_link = None
        self.links = []
        self.video_urls = []
        self.in_script = False

    def handle_starttag(self, tag, attrs):
        print "Start tag:", tag
        for attr in attrs:
            print "     attr:", attr
        if tag == 'a':
            self.current_link = Link(attrs)
        elif tag == 'img':
            if self.current_link:
                self.current_link.add_image(HtmlElement(attrs))
        elif tag == 'script':
            self.in_script = True

    def handle_endtag(self, tag):
        print "End tag  :", tag
        if tag == 'a':
            self.links.append(self.current_link)
            self.current_link = None
        elif tag == 'script':
            self.in_script = False

    def handle_data(self, data):
        print "Data     :", data
        if self.in_script:
            m = re.search(r'Uppod.*file: *"([^"]*)"', data)
            if m:
                print('Matches: {}'.format(m.group(1)))
                self.video_urls.append(m.group(1))

    def handle_comment(self, data):
        print "Comment  :", data

    def handle_entityref(self, name):
        c = unichr(name2codepoint[name])
        print "Named ent:", c

    def handle_charref(self, name):
        if name.startswith('x'):
            c = unichr(int(name[1:], 16))
        else:
            c = unichr(int(name))
        print "Num ent  :", c

    def handle_decl(self, data):
        print "Decl     :", data

    def get_channels(self, content):
        try:
            self.feed(content)
        except Exception, e:
            print('Exception thrown: {}'.format(e))
        return self.links

    def get_video_url(self, content):
        self.video_urls
        try:
            self.feed(content)
        except Exception, e:
            print('Exception thrown: {}'.format(e))
        return self.video_urls[0]

class Ruxet(object):
    base_url = 'http://tvin.su/'

    def __init__(self, args):
        self.log('__init__, args = ' + str(args))
        self.plugin_url = args[0]
        self.addon_handle = int(args[1])
        self.args = dict(urlparse.parse_qsl(args[2][1:]))
        print('Arguments: {}'.format(self.args))
        self.addon = xbmcaddon.Addon()
        xbmcplugin.setContent(self.addon_handle, 'videos')

    def log(self, msg, level=xbmc.LOGDEBUG):
        if level == xbmc.LOGERROR: msg += ' ,' + traceback.format_exc()
        xbmc.log(ADDON_ID + ' - ' + msg, level)

    def build_url(self, **kwargs):
        return '{0}?{1}'.format(self.plugin_url, urlencode(kwargs))

    def get_all_channels(self):
        r = requests.get(self.base_url + 'tv-channels/all')
        print('Ciao')
        print(r.status_code)
        print(r.encoding)
        print(r.content)
        parser = Parser()
        channels = parser.get_channels(r.content)
        for channel in channels:
            if not 'title' in channel.attributes: continue
            if not channel.image: continue
            print('channel attrs: {}'.format(channel.attributes))
            item = xbmcgui.ListItem(label=channel.image.attributes['alt'])
            channel_url = channel.attributes['href']
            icon = channel.image.attributes['src']
            url = self.build_url(mode='play', channel_url=channel_url)
            item.setArt({'thumb': icon, 'icon': icon, 'fanart':icon})
            item.setInfo('video', {})
            item.setProperty('IsPlayable', 'true')
            xbmcplugin.addDirectoryItem(handle=self.addon_handle, url=url, listitem=item)

        xbmcplugin.endOfDirectory(self.addon_handle)

    def playVideo(self, channel_url):
        self.log("Asked to play video at {}".format(channel_url))
        r = requests.get(self.base_url + channel_url)
        print(r.status_code)
        print(r.content)
        parser = Parser()
        video_url = parser.get_video_url(r.content)
        print('Got video URL: {}'.format(video_url))
        name = 'Todo'
        li = xbmcgui.ListItem(name, path=video_url)
        if 'm3u8' in video_url.lower():
            li.setProperty('inputstreamaddon','inputstream.adaptive')
            li.setProperty('inputstream.adaptive.manifest_type','hls')
        xbmcplugin.setResolvedUrl(self.addon_handle, True, li)
        

    def run(self):
        mode = self.args.get('mode', None)
        if mode == 'play':
            self.playVideo(self.args['channel_url'])
        else:
            self.get_all_channels()
